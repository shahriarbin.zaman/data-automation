# This program grabs data from the website: https://covid-tracker.chi-csm.ca which is in .json format


# Author: Shahriar Bin Zaman  #UCID: 30111988
# Version: 1.4


import urllib.request
import json
import csv
import requests
import datetime


def print_time():
    parser = datetime.datetime.now()
    return parser.strftime("%d-%m-%Y")


# Output>
# 03-02-2021 22:39:28

# Retrieve wasteWaterDataAB json  from the website (Covid Tracker Website)
# Saving that information in a csv file
# Returns a json and csv file
# This .json file has lots of information hence converted it to .csv file for future use


def wasteWaterDataAB():
    with urllib.request.urlopen(
            'https://covid-tracker-json.s3.us-west-2.amazonaws.com/wasteWaterAbData.json') as response:
        html = response.read()

    solditems = requests.get('https://covid-tracker-json.s3.us-west-2.amazonaws.com/wasteWaterAbData.json')
    values = solditems.json()

    with open(print_time() + ': wasteWaterAbData.json', 'w') as f:  # Save json file in local machine
        json.dump(values, f)

    with open(print_time() + ': wasteWaterAbData.json',
              "r") as f:  # Open the json file from local directory with read permission
        information = json.load(f)
        data = information["data"]

    with open(print_time() + ': wasteWaterAbData.csv',
              "w") as f:  # Iterate through .json and save corresponding data in a .csv file
        fieldnames = data[0].keys()
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for i in data:
            writer.writerow(i)


# Retrieve lastUpdatedCovidRates json  from the website (Covid Tracker Website)
# Returns a json file in local dir to store the information

def lastUpdatedCovidRates():
    with urllib.request.urlopen(
            'https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedCovidRates.json') as response:
        html = response.read()

    solditems = requests.get('https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedCovidRates.json')
    values = solditems.json()

    with open(print_time() + ': lastUpdatedCovidRates.json', 'w') as f:  # Save json file in local machine
        json.dump(values, f)


# Retrieve albertaMap json  from the website (Covid Tracker Website)
# Returns a json file in local dir to store the information
def albertaMap():
    with urllib.request.urlopen(
            'https://covid-tracker.chi-csm.ca/albertaMap.json') as response:
        html = response.read()

    solditems = requests.get('https://covid-tracker.chi-csm.ca/albertaMap.json')
    values = solditems.json()

    with open(print_time() + ': albertaMap.json', 'w') as f:  # Save json file in local machine
        json.dump(values, f)


# Retrieve lastUpdatedJhu json  from the website (Covid Tracker Website)
# Returns a json file in local dir to store the information
def lastUpdatedJhu():
    with urllib.request.urlopen(
            'https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedJhu.json') as response:
        html = response.read()

    solditems = requests.get('https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedJhu.json')
    values = solditems.json()

    with open(print_time() + ': lastUpdatedJhu.json', 'w') as f:  # Save json file in local machine
        json.dump(values, f)


# Retrieve lastUpdatedDaily json  from the website (Covid Tracker Website)
# Returns a json file in local dir to store the information
def lastUpdatedDaily():
    with urllib.request.urlopen(
            'https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedDaily.json') as response:
        html = response.read()

    solditems = requests.get('https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedDaily.json')
    values = solditems.json()

    with open(print_time() + ': lastUpdatedDaily.json', 'w') as f:
        json.dump(values, f)


# This function executes the program
def driver():
    wasteWaterDataAB()
    lastUpdatedCovidRates()
    lastUpdatedDaily()
    lastUpdatedJhu()
    albertaMap()


# Driver function is being called here
driver()
