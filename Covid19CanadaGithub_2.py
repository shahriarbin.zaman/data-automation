import requests
import datetime
import urllib
import datetime

from urllib.request import Request, urlopen



import urllib.request


def print_time():
    parser = datetime.datetime.now()
    return parser.strftime("%d-%m-%Y")


# the below url are taken from (CovidTimelineCanada/data/pt)
def driver():

    url_cases = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/cases_pt.csv'
    urllib.request.urlretrieve(url_cases, './data/Covid19CanadaGithub/daily_cases/ ' + print_time() + '.csv')

    url_deaths = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/deaths_pt.csv'
    urllib.request.urlretrieve(url_deaths, './data/Covid19CanadaGithub/daily_death/ ' + print_time() + '.csv')

    url_hospitalizations = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/hospitalizations_pt.csv'
    urllib.request.urlretrieve(url_hospitalizations,
                               './data/Covid19CanadaGithub/daily_hospitalization/ ' + print_time() + '.csv')


    url_tests_completed = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/tests_completed_pt.csv'
    urllib.request.urlretrieve(url_tests_completed,
                               './data/Covid19CanadaGithub/daily_test/ ' + print_time() + '.csv')

    url_vaccine_administration_dose_1 = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_dose_1_pt.csv'
    urllib.request.urlretrieve(url_vaccine_administration_dose_1,
                               './data/Covid19CanadaGithub/daily_vaccine_administration/dose_1: ' + print_time() + '.csv')

    url_vaccine_administration_dose_2 = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_dose_2_pt.csv'
    urllib.request.urlretrieve(url_vaccine_administration_dose_2,
                               './data/Covid19CanadaGithub/daily_vaccine_administration/dose_2: ' + print_time() + '.csv')

    url_vaccine_administration_dose_3 = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_dose_3_pt.csv'
    urllib.request.urlretrieve(url_vaccine_administration_dose_3,
                               './data/Covid19CanadaGithub/daily_vaccine_administration/dose_3: ' + print_time() + '.csv')

    url_vaccine_administration_total_doses = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_total_doses_pt.csv'
    urllib.request.urlretrieve(url_vaccine_administration_total_doses,
                               './data/Covid19CanadaGithub/daily_vaccine_administration/total_doses: ' + print_time() + '.csv')

    url_vaccine_coverage_dose_1 = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_1_pt.csv'
    urllib.request.urlretrieve(url_vaccine_coverage_dose_1,
                               './data/Covid19CanadaGithub/daily_vaccine_coverage/dose_1: ' + print_time() + '.csv')

    url_vaccine_coverage_dose_2 = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_2_pt.csv'
    urllib.request.urlretrieve(url_vaccine_coverage_dose_2,
                               './data/Covid19CanadaGithub/daily_vaccine_coverage/dose_2: ' + print_time() + '.csv')

    url_vaccine_coverage_dose_3 = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_3_pt.csv'
    urllib.request.urlretrieve(url_vaccine_coverage_dose_3,
                               './data/Covid19CanadaGithub/daily_vaccine_coverage/dose_3: ' + print_time() + '.csv')

    url_vaccine_coverage_dose_4 = 'https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_4_pt.csv'
    urllib.request.urlretrieve(url_vaccine_coverage_dose_4,
                               './data/Covid19CanadaGithub/daily_vaccine_coverage/dose_4: ' + print_time() + '.csv')

driver()
