import urllib
import datetime
import urllib.request


def print_time():
    parser = datetime.datetime.now()
    return parser.strftime("%d-%m-%Y")


def wasteWaterDataAB():
    myUrl = 'https://covid-tracker-json.s3.us-west-2.amazonaws.com/wasteWaterAbData.json'  # Your url goes here
    urllib.request.urlretrieve(myUrl, './data/CovidTrackerWebsite/wasteWaterAbData: ' + print_time() + '.json')


def lastUpdatedCovidRates():
    myUrl = 'https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedCovidRates.json'  # Your url goes here
    urllib.request.urlretrieve(myUrl, './data/CovidTrackerWebsite/lastUpdatedCovidRates: ' + print_time() + '.json')


def albertaMap():
    myUrl = 'https://covid-tracker.chi-csm.ca/albertaMap.json'  # Your url goes here
    urllib.request.urlretrieve(myUrl, './data/CovidTrackerWebsite/albertaMap: ' + print_time() + '.json')


def lastUpdatedJhu():
    myUrl = 'https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedJhu.json'  # Your url goes here
    urllib.request.urlretrieve(myUrl, './data/CovidTrackerWebsite/lastUpdatedJhu: ' + print_time() + '.json')


def lastUpdatedDaily():
    myUrl = 'https://covid-tracker-json.s3.us-west-2.amazonaws.com/lastUpdatedDaily.json'  # Your url goes here
    urllib.request.urlretrieve(myUrl, './data/CovidTrackerWebsite/lastUpdatedDaily: ' + print_time() + '.json')


wasteWaterDataAB()
lastUpdatedCovidRates()
albertaMap()
lastUpdatedJhu()
lastUpdatedJhu()