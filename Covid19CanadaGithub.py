import requests
import datetime


def print_time():
    parser = datetime.datetime.now()
    return parser.strftime("%d-%m-%Y" )
# the below url are taken from (CovidTimelineCanada/data/pt)

url_cases='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/cases_pt.csv'

url_deaths='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/deaths_pt.csv'
url_hospitalizations='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/hospitalizations_pt.csv'
url_icu='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/icu_pt.csv'
url_tests_completed='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/tests_completed_pt.csv'

url_vaccine_administration_dose_1='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_dose_1_pt.csv'
url_vaccine_administration_dose_2='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_dose_2_pt.csv'
url_vaccine_administration_dose_3='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_dose_3_pt.csv'
url_vaccine_administration_total_doses='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_administration_total_doses_pt.csv'


url_vaccine_coverage_dose_1='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_1_pt.csv'
url_vaccine_coverage_dose_2='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_2_pt.csv'
url_vaccine_coverage_dose_3='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_3_pt.csv'
url_vaccine_coverage_dose_4='https://raw.githubusercontent.com/ccodwg/CovidTimelineCanada/main/data/pt/vaccine_coverage_dose_4_pt.csv'

url_vaccine_alberta='https://www.alberta.ca/data/stats/lga-coverage.csv'
url_cases_alberta_lha='https://www.alberta.ca/data/stats/covid-19-alberta-statistics-map-data.csv'
url_cases_alberta='https://www.alberta.ca/data/stats/covid-19-alberta-statistics-data.csv'

url_calgary_weather='https://calgary.weatherstats.ca/download.html/'

url_dict={'daily_cases: ':url_cases, 'death_cases: ':url_deaths, 'daily_hospitalizations: ': url_hospitalizations, 'daily_icu: ': url_icu,
          'tests_completed: ': url_tests_completed,
          'vaccine_administration_total_doses: ': url_vaccine_administration_total_doses, 'vaccine_administration_dose_1: ':url_vaccine_administration_dose_1,
          'vaccine_administration_dose_2: ':url_vaccine_administration_dose_2, 'vaccine_administration_dose_3: ':url_vaccine_administration_dose_3,
          'vaccine_coverage_dose_1: ':url_vaccine_coverage_dose_1, 'vaccine_coverage_dose_2: ':url_vaccine_coverage_dose_2,
          'vaccine_coverage_dose_3: ':url_vaccine_coverage_dose_3, 'vaccine_coverage_dose_4: ':url_vaccine_coverage_dose_4,
          'vaccine_alberta: ':url_vaccine_alberta, 'cases_alberta_lha: ': url_cases_alberta_lha, 'calgary_weather: ': url_calgary_weather}

print('=====> start downloading <=====')
for url_name, url in url_dict.items():
    r = requests.get(url, allow_redirects=True)
    open(url_name+print_time(), 'wb').write(r.content)
    print('{} is downloaded'.format(url_name))